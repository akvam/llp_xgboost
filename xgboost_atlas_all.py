import sys
import xgboost as xgb
#from xgboost import XGBClassifier
import os
import pandas as pd
import sklearn
from sklearn.model_selection import train_test_split
from sklearn.feature_selection import SelectFromModel
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve
from matplotlib import pyplot as plt
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.colors as colors
import datetime
from scipy.stats import pearsonr

print (sys.version)
print (xgb.__version__)
print (pd.__version__)
print (sklearn.__version__)

# iregion = 0 is barrel, = 1 is endcap
iregion = 0
sregion = str(iregion)
# this is a cat of hss and data.csv
# first make one file for barrel and one file for endcap

# put input files into list
input_file = []
if (iregion == 0):
        input_file.append('panda_barrel_data_2015_2018-2021-07-19.csv')
        input_file.append('panda_barrel_hss125-5-2021-07-18.csv')
        input_file.append('panda_barrel_hss125-15-2021-07-18.csv')
        input_file.append('panda_barrel_hss125-35-2021-07-18.csv') 
        input_file.append('panda_barrel_hss125-55-2021-07-18.csv')
#        input_file.append('panda_barrel_hss60-5-2021-06-16.csv')
#        input_file.append('panda_barrel_hss60-15-2021-06-16.csv')
#        input_file.append('panda_barrel_hss200-50-2021-06-16.csv')
#        input_file.append('panda_barrel_hss400-100-2021-06-16.csv')
#        input_file.append('panda_barrel_hss600-50-2021-06-16.csv')
#        input_file.append('panda_barrel_hss600-150-2021-06-16.csv')
#        input_file.append('panda_barrel_hss600-275-2021-06-16.csv')
#        input_file.append('panda_barrel_hss1000-50-2021-06-16.csv')
#        input_file.append('panda_barrel_hss1000-275-2021-06-16.csv')
#        input_file.append('panda_barrel_hss1000-475-2021-06-16.csv')

elif (iregion == 1):
        input_file.append('panda_endcap_data_2015_2018-2021-07-19.csv')
        input_file.append('panda_endcap_hss125-5-2021-07-18.csv')
        input_file.append('panda_endcap_hss125-15-2021-07-18.csv')
        input_file.append('panda_endcap_hss125-35-2021-07-18.csv')
        input_file.append('panda_endcap_hss125-55-2021-07-18.csv')
#        input_file.append('panda_endcap_hss60-5-2021-06-16.csv')
#        input_file.append('panda_endcap_hss60-15-2021-06-16.csv')
#        input_file.append('panda_endcap_hss200-50-2021-06-16.csv')
#        input_file.append('panda_endcap_hss400-100-2021-06-16.csv')
#        input_file.append('panda_endcap_hss600-50-2021-06-16.csv')
#        input_file.append('panda_endcap_hss600-150-2021-06-16.csv')
#        input_file.append('panda_endcap_hss600-275-2021-06-16.csv')
#        input_file.append('panda_endcap_hss1000-50-2021-06-16.csv')
#        input_file.append('panda_endcap_hss1000-275-2021-06-16.csv')
#        input_file.append('panda_endcap_hss1000-475-2021-06-16.csv')

df_samples = []
print (len(input_file))

# make panda df for each file (dfs)
# read csv
# note the for loop is at at end
if (iregion == 0):
    list_of_dfs = [pd.read_csv(i, header = 0,dtype={'MSVtx_barrel_hits_g5track_drmin':float,\
'MSVtx_barrel_hits_vec_jet_drmin':float, 'MSVtx_barrel_hits_vec_jet_pt':float,\
'MSVtx_barrel_hits_vec_jet_nmdt':float, 'MSVtx_barrel_hits_vec_jet_nrpc':float,\
'MSVtx_barrel_hits_dr_mean_MSeg':float, 'MSVtx_barrel_hits_dr_rms_MSeg':float,\
'MSVtx_barrel_hits_dr_mean_MSTracklet':float, 'MSVtx_barrel_hits_dr_rms_MSTracklet':float,\
'MSVtx_barrel_hits_g5track_ptsum_opposite':float,'MSVtx_barrel_hits_met_dphi':float,\
'h_mass':int,'s_mass':int,'bdt_sample_type':int, 'bdt_target':int}) for i in input_file]

elif (iregion == 1):
    list_of_dfs =  [pd.read_csv(i, header = 0,dtype={'MSVtx_endcap_hits_g5track_drmin':float,\
'MSVtx_endcap_hits_vec_jet_drmin':float, 'MSVtx_endcap_hits_vec_jet_pt':float,\
'MSVtx_endcap_hits_vec_jet_nmdt':float, 'MSVtx_endcap_hits_vec_jet_ntgc':float,\
'MSVtx_endcap_hits_dr_mean_MSeg':float, 'MSVtx_endcap_hits_dr_rms_MSeg':float,\
'MSVtx_endcap_hits_dr_mean_MSTracklet':float, 'MSVtx_endcap_hits_dr_rms_MSTracklet':float,\
'MSVtx_endcap_hits_nMSeg_eta':int, 'MSVtx_endcap_hits_nMSTracklet_eta':int,\
'MSVtx_endcap_hits_g5track_ptsum_opposite':float,'MSVtx_endcap_hits_met_dphi':float,\
'h_mass':int,'s_mass':int,'bdt_sample_type':int, 'bdt_target':int}) for i in input_file]

print (len(list_of_dfs))
print (list_of_dfs[0].shape[0])
print (list_of_dfs[0].shape[1])
print (type(list_of_dfs))
print (type(list_of_dfs[0]))
# so the data for each sample is contained in list_of_dfs
#exit()

# make a copy for the train_test_sample
# make a copy for the mass_test_sample
train_test_samples=list_of_dfs.copy()
mass_test_samples = list_of_dfs.copy()
print (type(train_test_samples))
print (type(mass_test_samples[0]))
#exit()

# next reduce each df as appropriate
# for train_test, this means we make a sample with Nbkg = sum of Nsig
#
if (iregion == 0):
    for i in range(len(train_test_samples)):
        temp = train_test_samples[i].copy()
# print the first two rows
#        if (i == 0): print (temp.iloc[0,0:2])
#        if (i == 0): print (temp.iloc[1,0:2])
# shuffle 
#        if (i == 0): temp = temp.sample(frac=1)
#        if (i == 0): temp = temp.sample(frac=1).reset_index(drop=True)
#print the first two rows
#        if (i == 0): print (temp.iloc[0,0:2])
#        if (i == 0): print (temp.iloc[1,0:2])


# shuffle and reset the index, because we filter on it below
        temp = temp.sample(frac=1).reset_index(drop=True)

# i = 0 is background
        if (i == 0): train_test_samples[i] = temp[temp.index%3 == 0]
        if (i == 1): train_test_samples[i] = temp[temp.index%1 == 0]
        if (i == 2): train_test_samples[i] = temp[temp.index%1 == 0]
        if (i == 3): train_test_samples[i] = temp[temp.index%1 == 0]
        if (i == 4): train_test_samples[i] = temp[temp.index%1 == 0]
#        if (i == 5): train_test_samples[i] = temp[temp.index%2 == 0]
#        if (i == 6): train_test_samples[i] = temp[temp.index%1 == 0]
#        if (i == 7): train_test_samples[i] = temp[temp.index%1 == 0]
#        if (i == 8): train_test_samples[i] = temp[temp.index%1 == 0]
#        if (i == 9): train_test_samples[i] = temp[temp.index%3 == 0]
#        if (i == 10): train_test_samples[i] = temp[temp.index%4 == 0]
#        if (i == 11): train_test_samples[i] = temp[temp.index%1 == 0]
#        if (i == 12): train_test_samples[i] = temp[temp.index%3 == 0]
#        if (i == 13): train_test_samples[i] = temp[temp.index%3 == 0]
#        if (i == 14): train_test_samples[i] = temp[temp.index%1 == 0]

    print ('train and test samples')
    for i in range(len(train_test_samples)):
        print(train_test_samples[i].shape[0])

elif (iregion == 1):
    for i in range(len(train_test_samples)):
        temp = train_test_samples[i].copy()
        temp = temp.sample(frac=1).reset_index(drop=True)
# i = 0 is background
# fix these for endcap
        if (i == 0): train_test_samples[i] = temp[temp.index%3 == 0]
        if (i == 1): train_test_samples[i] = temp[temp.index%1 == 0]
        if (i == 2): train_test_samples[i] = temp[temp.index%1 == 0]
        if (i == 3): train_test_samples[i] = temp[temp.index%2 == 0]
        if (i == 4): train_test_samples[i] = temp[temp.index%1 == 0]
#        if (i == 5): train_test_samples[i] = temp[temp.index%2 == 0]
#        if (i == 6): train_test_samples[i] = temp[temp.index%1 == 0]
#        if (i == 7): train_test_samples[i] = temp[temp.index%1 == 0]
#        if (i == 8): train_test_samples[i] = temp[temp.index%1 == 0]
#        if (i == 9): train_test_samples[i] = temp[temp.index%1 == 0]
#        if (i == 10): train_test_samples[i] = temp[temp.index%1 == 0]
#        if (i == 11): train_test_samples[i] = temp[temp.index%1 == 0]
#        if (i == 12): train_test_samples[i] = temp[temp.index%1 == 0]
#        if (i == 13): train_test_samples[i] = temp[temp.index%1 == 0]
#        if (i == 14): train_test_samples[i] = temp[temp.index%1 == 0]

    print ('train and test samples')
    for i in range(len(train_test_samples)):
        print(train_test_samples[i].shape[0])

#exit()

# now concat all the pd
#frames = [train_test_samples[0],train_test_samples[1],train_test_samples[2],train_test_samples[3],\
#          train_test_samples[4],train_test_samples[5],train_test_samples[6],train_test_samples[7],\
#          train_test_samples[8],train_test_samples[9],train_test_samples[10],train_test_samples[11],\
#          train_test_samples[12],train_test_samples[13],train_test_samples[14]]
frames = [train_test_samples[0],train_test_samples[1],train_test_samples[2],train_test_samples[3],\
          train_test_samples[4]]
df_all = pd.concat(frames, ignore_index=True)
# shuffle the concat data
df_all = df_all.sample(frac=1).reset_index(drop=True)
# done - see df_all.copy below

# also make test samples that are mass specific
# next reduce each df as appropriate
if (iregion == 0):
    for i in range(len(mass_test_samples)):
        temp = mass_test_samples[i].copy()
# shuffle and reset the index, because we filter on it below
        temp = temp.sample(frac=1).reset_index(drop=True)

# i = 0 is background
        if (i == 0): mass_test_samples[i] = temp[temp.index%10 == 0]
        if (i == 1): mass_test_samples[i] = temp[temp.index%1 == 0]
        if (i == 2): mass_test_samples[i] = temp[temp.index%1 == 0]
        if (i == 3): mass_test_samples[i] = temp[temp.index%1 == 0]
        if (i == 4): mass_test_samples[i] = temp[temp.index%1 == 0]
#        if (i == 5): mass_test_samples[i] = temp[temp.index%2 == 0]
#        if (i == 6): mass_test_samples[i] = temp[temp.index%1 == 0]
#        if (i == 7): mass_test_samples[i] = temp[temp.index%1 == 0]
#        if (i == 8): mass_test_samples[i] = temp[temp.index%1 == 0]
#        if (i == 9): mass_test_samples[i] = temp[temp.index%3 == 0]
#        if (i == 10): mass_test_samples[i] = temp[temp.index%4 == 0]
#        if (i == 11): mass_test_samples[i] = temp[temp.index%1 == 0]
#        if (i == 12): mass_test_samples[i] = temp[temp.index%3 == 0]
#        if (i == 13): mass_test_samples[i] = temp[temp.index%3 == 0]
#        if (i == 14): mass_test_samples[i] = temp[temp.index%1 == 0]

    print ('test mass samples')
    for i in range(len(mass_test_samples)):
        print(mass_test_samples[i].shape[0])

elif (iregion == 1):
    for i in range(len(mass_test_samples)):
        temp = mass_test_samples[i].copy()
        temp = temp.sample(frac=1).reset_index(drop=True)
# i = 0 is background
# fix these for endcap
        if (i == 0): mass_test_samples[i] = temp[temp.index%12 == 0]
        if (i == 1): mass_test_samples[i] = temp[temp.index%1 == 0]
        if (i == 2): mass_test_samples[i] = temp[temp.index%1 == 0]
        if (i == 3): mass_test_samples[i] = temp[temp.index%2 == 0]
        if (i == 4): mass_test_samples[i] = temp[temp.index%1 == 0]
#        if (i == 5): mass_test_samples[i] = temp[temp.index%2 == 0]
#        if (i == 6): mass_test_samples[i] = temp[temp.index%1 == 0]
#        if (i == 7): mass_test_samples[i] = temp[temp.index%1 == 0]
#        if (i == 8): mass_test_samples[i] = temp[temp.index%1 == 0]
#        if (i == 9): mass_test_samples[i] = temp[temp.index%1 == 0]
#        if (i == 10): mass_test_samples[i] = temp[temp.index%1 == 0]
#        if (i == 11): mass_test_samples[i] = temp[temp.index%1 == 0]
#        if (i == 12): mass_test_samples[i] = temp[temp.index%1 == 0]
#        if (i == 13): mass_test_samples[i] = temp[temp.index%1 == 0]
#        if (i == 14): mass_test_samples[i] = temp[temp.index%1 == 0]

    print ('test mass samples')
    for i in range(len(mass_test_samples)):
        print(mass_test_samples[i].shape[0])


# now combine each mass with signal and background
# 0 is background
print ('one signal + background samples')
for i in range(1,len(mass_test_samples)):
     mass_test_samples[i] = pd.concat([mass_test_samples[i],mass_test_samples[0]], ignore_index=True)
     print (mass_test_samples[i].shape[0])
#exit()

# now cat them together
#df_all_temp = pd.concat([df_reduced_signal1,df_reduced_signal2], ignore_index=True)
#df_all =pd.concat([df_all_temp,df_reduced_background1], ignore_index=True)

# and do some checks
df_temp = df_all.loc[ df_all['bdt_sample_type'] == 10]
print ('df all data ',df_temp.shape[0])
df_temp = df_all.loc[ ((df_all['bdt_sample_type'] == 20) & (df_all['s_mass'] == 55)) ]
print ('df all signal 1 ',df_temp.shape[0])
df_temp = df_all.loc[ ((df_all['bdt_sample_type'] == 20) & (df_all['s_mass'] == 5)) ]
print ('df all signal 1 ',df_temp.shape[0])


#df = pd.read_csv(input_file, header = 0)
# check dtypes
print (df_all['bdt_target'].dtypes)
#print (df_all['MSVtx_endcap_hits_g5track_drmin'].dtypes)
print (df_all.dtypes)
print (df_all.shape)
print (df_all.shape[0],df_all.shape[1])
print (df_all.size)
#print ('[0,:],[1,:]')
#print (df_all.loc[0,:])
#print (df_all.loc[1,:])
print (df_all)
print ('info')
print (df_all.info())
print ('start')
#exit()

# should probably use something else, but df is the end result of
# of combining background and all signal files
df = df_all.copy()
# for nathan
# df.to_csv('barrel_all_mass_071021.csv',index=False)
# exit()

# create a signal only sample - for now all signal
# create a background only sample - for now 10% of background
# mix them together
#if (iregion == 0):
#    df_signal = df_all[ df_all['bdt_sample_type'] == 20]    print ('all signal ',df_signal.shape[0])
#    df_background = df_all [ df_all['bdt_sample_type'] == 10]
#    print ('all background ',df_background.shape[0])
# use 50 for 125,55
# use 5 for 125,5
#    df_reduced_background = df_background[df_background.index%5 == 0]
#    print ('reduced background ',df_reduced_background.shape[0])
#    df = pd.concat([df_signal,df_reduced_background])
#elif (iregion == 1):
#    df_signal = df_all[ df_all['bdt_sample_type'] == 20]
#    print ('all signal ',df_signal.shape[0])
#    df_background = df_all [ df_all['bdt_sample_type'] == 10]
#    print ('all background ',df_background.shape[0])
# use 5 for 125,55
# use 4 for 125,5
#    df_reduced_background = df_background[df_background.index%4 == 0]
#    print ('reduced background ',df_reduced_background.shape[0])
#    df = pd.concat([df_signal,df_reduced_background])

#exit()
#print (df.loc[0,0])
#print (df.loc[1,0])

# change column order - actually dont need to do this since target column can be specified below
#df = df[['bdt_target','MSVtx_barrel_hits_g5track_drmin','MSVtx_barrel_hits_vec_jet_drmin','MSVtx_barrel_hits_vec_jet_nmdt','MSVtx_barrel_hits_vec_jet_pt']]

# shuffle all the data
# not sure we need to do this again, since we did it above
# df is the name we using going forward
df = df.sample(frac=1).reset_index(drop=True)
#print ('[0,:],[1,:]')
#print (df.loc[0,:])
#print (df.loc[1,:])
print (df)
print ('df',df.shape[0])
# set the training and target, set the test and target
#y = df.iloc[:,5]
#X = df.iloc[:,1:5]

# note new location
y = df.iloc[:,11]
# use this if you want to select all columns
#X = df.iloc[:,:-1]
# use this if you want to select only certain columns to use
if (iregion == 0):
#    X = df.iloc[:, [0,1,2,3,5,6,8,9,10]]
    X = df.iloc[:, [0,1,2,3,5,6,8,9]]
elif (iregion == 1):
#    X = df.iloc[:, [0,1,2,3,5,6,8,9,10]]
    X = df.iloc[:, [0,1,2,3,5,6,8,9]]

print ('X',X.shape[0])
print ('y',y.shape[0])
#print (y)
#print (X)
# i think this shuffles again, in addition to above df.sample
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.3, random_state = 0)
# it's a df
# print (type(y_test))

# make a ntuple for df
# make numpy array for plotting
# z is all df
z = df.to_numpy()
print (type(z))
print (np.shape(z))
#    print (z[0,:])
#    print (z[1,:])
print ('df',len(z))

# make some arrays to calculate correlation
zjetpt = z[:,10]
zmdt = z[:,8]
zjetdrmin = z[:,7]
ztrackdrmin = z[:,4]

corr, _ = pearsonr(zjetdrmin, ztrackdrmin)
print('Pearsons correlation: %.3f' % corr)
corr, _ = pearsonr(zjetdrmin, zjetpt)
print('Pearsons correlation: %.3f' % corr)
corr, _ = pearsonr(zmdt, zjetdrmin)
print('Pearsons correlation: %.3f' % corr)
corr, _ = pearsonr(zmdt, zjetpt)
print('Pearsons correlation: %.3f' % corr)
corr, _ = pearsonr(zmdt, zmdt)
print('Pearsons correlation: %.3f' % corr)

#exit()

# and y
z_yall = y.to_numpy()
print (np.shape(z_yall))
# count number of signal events in y
count1 = 0
count0 = 0
for i in range(len(z_yall)):
    if (z_yall[i] == 1): count1 = count1 + 1
    if (z_yall[i] == 0): count0 = count0 + 1
print ('number of signal events in y: ',count1)
print ('number of background events in y: ',count0)

# and y_test
z_ytest = y_test.to_numpy()
print (np.shape(z_ytest))
# count number of signal events in y_test
count1 = 0
count0 = 0
for i in range(len(z_ytest)):
    if (z_ytest[i] == 1): count1 = count1 + 1
    if (z_ytest[i] == 0): count0 = count0 + 1
print ('number of signal events in y_test: ',count1)
print ('number of background events in y_test: ',count0)

# the number of signal events total and in test is correct

# and X_test
z_xtest = X_test.to_numpy()
print (np.shape(z_xtest))
print (len(z_xtest))
print ('z_xtest')
print (z_xtest.shape[0])


# specify parameters via map
#param = {'max_depth':2, 'eta':1, 'objective':'binary:logistic' }

# out of box parameters for now
classifier = xgb.XGBClassifier(use_label_encoder=False)
# we might consider using just DMatrix.  will try that next
#  but XGBClassfier likely calss dmatrix
# https://stackoverflow.com/questions/47152610/what-is-the-difference-between-xgb-train-and-xgb-xgbregressor-or-xgb-xgbclassif

#config = xgb.get_config()
#assert config['verbosity'] == 2
#print (config)


#classifier.fit(X_train, y_train)
eval_set = [(X_train, y_train), (X_test,y_test)]
classifier.fit(X_train, y_train, early_stopping_rounds=20, eval_metric=["error","logloss"], eval_set=eval_set,verbose=True)
#print ('eval')
#print (type(eval_set))
#print (eval_set)
#exit()

y_pred = classifier.predict(X_test,validate_features=True)
accuracy = accuracy_score(y_test,y_pred)
print ('accuracy',accuracy)
print (type(X_train))
print (type(y_pred))
y_pred_proba = classifier.predict_proba(X_test)
print (type(y_pred_proba))
print (np.shape(y_pred_proba))

# this list contains a list of y_pred_proba_mass
y_pred_proba_mass_list = []

# now test on each mass sample
for i in range(1,len(mass_test_samples)):
# note new location
    y_test_mass = mass_test_samples[i].iloc[:,11]
# use this if you want to select only certain columns to use
    if (iregion == 0):
#    X = df.iloc[:, [0,1,2,3,5,6,8,9,10]]
        X_test_mass = mass_test_samples[i].iloc[:, [0,1,2,3,5,6,8,9]]
#        X_test_mass = mass_test_samples[i].iloc[:, [0,1,2,3,5,6,8,9,10]]
    elif (iregion == 1):
        X_test_mass = mass_test_samples[i].iloc[:, [0,1,2,3,5,6,8,9]]
#        X_test_mass = mass_test_samples[i].iloc[:, [0,1,2,3,5,6,8,9,10]]
    y_pred_mass = classifier.predict(X_test_mass,validate_features=True)
    accuracy = accuracy_score(y_test_mass,y_pred_mass)
    print ('i',i,'accuracy',accuracy)
    y_pred_proba_mass = classifier.predict_proba(X_test_mass,validate_features=True)
    print (type(y_pred_proba_mass))
    print (np.shape(y_pred_proba_mass))
# sanity test
#    if (i == 1):
#        print (X_test_mass.iloc[0,2:5])
#        print (mass_test_samples[i].iloc[0,2:5])
#        print (y_pred_proba_mass[0,0]) 
#exit()
    y_pred_proba_mass_list.append(y_pred_proba_mass)
print (len(y_pred_proba_mass_list))


# retrieve performance metrics
# results = model.evals_result()
# epochs = len(results['validation_0']['error'])
# x_axis = range(0, epochs)
# see plots option 9

#print ('y_pred', 'y_prob')
##print (y_pred)
#for i in range(len(y_pred)):
#    if (y_pred[i] == 1): 
#        print (y_pred[i],y_pred_proba[i,:])

# count number of signal events in y_test
count = 0
for i in range(len(z_ytest)):
    if (z_ytest[i] == 1):
#        print(z[i],y_pred[i],y_pred_proba[i,:]) 
        count = count + 1
print ('number of signal events in y_test: ',count)
# count number of signal events in y_test
count = 0
for i in range(len(z_ytest)):
    if (z_ytest[i] == 0):
#        print(z[i],y_pred[i],y_pred_proba[i,:])
        count = count + 1
print ('number of background events in y_test: ',count)

# exit()
# the number of signal events total and in test is correct

#thresholds = np.sort(classifier.feature_importances_)
#for thresh in thresholds:
##    print (thresh)
#    # select features using threshold
#    selection = SelectFromModel(classifier, threshold=thresh, prefit=True)
#    select_X_train = selection.transform(X_train)
#    # train model
#    selection_model = xgb.XGBClassifier(use_label_encoder=False)
#    selection_model.fit(select_X_train, y_train)
#    # eval model
#    select_X_test = selection.transform(X_test)
#    predictions = selection_model.predict(select_X_test)
#    accuracy = accuracy_score(y_test, predictions)
#    print("Thresh=%.3f, n=%d, Accuracy: %.2f%%" % (thresh, select_X_train.shape[1], accuracy*100.0))


# save model?
classifier.save_model('model_file_name.json')
config = classifier.get_params()
print ('get_params output')
print (config)

# probably better metrics, something for now
from sklearn.metrics import accuracy_score
print (type(y_test),type(y_pred))
accuracy = accuracy_score(y_test, y_pred, normalize=True)
print(accuracy)
num_accuracy = accuracy_score(y_test, y_pred, normalize=False)
print(num_accuracy)

print (classifier.get_booster().get_score(importance_type='gain'))
print (classifier.feature_importances_)
#plt.bar(range(len(classifier.feature_importances_)), classifier.feature_importances_)
print ('done before hists')


# 0 importance
# 1 all plots
# 2 y_pred_prob
# 3 rms and ave
# 4 drmin and pt and ptsum
# 5 nrpc, nmdt, nmseg, nmtracklet
# 6 met
# 8 pdf output
# 9 results

# choose matplotlib output
ichoose = 10

if (ichoose == 0):
    xgb.plot_importance(classifier)
    plt.tight_layout()
    plt.show()

elif (ichoose == 1):
    pass

elif (ichoose == 2):
    #pass
    fig, ax = plt.subplots(2,2)
    print (np.shape(y_pred))
    ax[0,0].hist(y_pred[:], bins=50, range=(0,1.01), histtype='step', color='b', log='True')
    ax[0,0].set(title='y_pred', xlabel='y_pred', ylabel='number')
    ax[1,0].hist(y_pred_proba[:,1], bins=50, range=(0,1.01), histtype='step', color='r', log='True')
    ax[1,0].hist(y_pred_proba[:,0], bins=50, range=(0,1.01), histtype='step', color='b', log='True')
    ax[1,0].set(title='y_pred_prob', xlabel='y_pred_prob', ylabel='number')

# make numpy array for plotting
#    print (z_ytest[:])
    print ('y_test len',len(z_ytest))
    signal = np.array([])
    background = np.array([])
    for i in range(len(z_ytest)):
      if (z_ytest[i] == 1):
          signal = np.append(signal,y_pred_proba[i,1])
      elif (z_ytest[i] == 0):
          background = np.append(background,y_pred_proba[i,0])
      else:
          pass
    background = 1. - background
    ax[1,1].hist(signal, bins=50, range=(0,1.01), histtype='step', color='r', log='True')
    ax[1,1].hist(background, bins=50, range=(0,1.01), histtype='step', color='b', log='True')
    ax[1,1].set(title='y_pred_prob using y_test', xlabel='y_pred_prob using y_test', ylabel='number')
    plt.tight_layout()
    plt.show()

elif (ichoose == 3):
    fig, ax = plt.subplots(2,2)
    signal = np.array([])
    background = np.array([])
    for i in range(len(z)):
        if (z[i,11] == 1):
            signal = np.append(signal,z[i,0])
        else:
            background = np.append(background,z[i,0])
    ax[0,0].hist(signal, bins=50, range=(0,1), histtype='step', color='r', density='True')
    ax[0,0].hist(background, bins=50, range=(0,1), histtype='step', color='b', density='True')
    ax[0,0].set(title='ms tracklet mean', xlabel='ms tracklet mean', ylabel='number')

    signal = np.array([])
    background = np.array([])
    for i in range(len(z)):
        if (z[i,11] == 1):
            signal = np.append(signal,z[i,2])
        else:
            background = np.append(background,z[i,2])
    ax[0,1].hist(signal, bins=50, range=(0,1), histtype='step', color='r', density='True')
    ax[0,1].hist(background, bins=50, range=(0,1), histtype='step', color='b', density='True')
    ax[0,1].set(title='ms tracklet rms', xlabel='ms tracklet rms', ylabel='number')

    signal = np.array([])
    background = np.array([])
    for i in range(len(z)):
        if (z[i,11] == 1):
            signal = np.append(signal,z[i,1])
        else:
            background = np.append(background,z[i,1])
    ax[1,0].hist(signal, bins=50, range=(0,1), histtype='step', color='r', density='True')
    ax[1,0].hist(background, bins=50, range=(0,1), histtype='step', color='b', density='True')
    ax[1,0].set(title='mseg mean', xlabel='mseg mean', ylabel='number')
    signal = np.array([])
    background = np.array([])
    for i in range(len(z)):
        if (z[i,11] == 1):
            signal = np.append(signal,z[i,3])
        else:
            background = np.append(background,z[i,3])
    ax[1,1].hist(signal, bins=50, range=(0,1), histtype='step', color='r', density='True')
    ax[1,1].hist(background, bins=50, range=(0,1), histtype='step', color='b', density='True')
    ax[1,1].set(title='mseg rms', xlabel='mseg rms', ylabel='number')
    plt.tight_layout()
    plt.show()

elif (ichoose == 4):
    fig, ax = plt.subplots(2,2)
    signal = np.array([])
    background = np.array([])
    for i in range(len(z)):
        if (z[i,11] == 1): 
            signal = np.append(signal,z[i,9])
        else:
            background = np.append(background,z[i,9])
    print (len(signal))
    print (len(background))
    ax[0,0].hist(signal, bins=50, range=(0,3), histtype='step', color='r', density='True')
    ax[0,0].hist(background, bins=50, range=(0,3), histtype='step', color='b', density='True')
    ax[0,0].set(title='jet drmin', xlabel='jet drmin', ylabel='number')

    signal = np.array([])
    background = np.array([])
    for i in range(len(z)):
        if (z[i,11] == 1):
            signal = np.append(signal,z[i,4])
        else:
            background = np.append(background,z[i,4])
    ax[0,1].hist(signal, bins=50, range=(0,3), histtype='step', color='r', density='True')
    ax[0,1].hist(background, bins=50, range=(0,3), histtype='step', color='b', density='True')
    ax[0,1].set(title='track drmin', xlabel='track drmin', ylabel='number')

    signal = np.array([])
    background = np.array([])
    for i in range(len(z)):
        if (z[i,11] == 1):
            signal = np.append(signal,z[i,12])
        else:
            background = np.append(background,z[i,12])
    ax[1,0].hist(signal, bins=50, range=(0,500), histtype='step', color='r', density='True')
    ax[1,0].hist(background, bins=50, range=(0,500), histtype='step', color='b', density='True')
    ax[1,0].set(title='closest jet pt', xlabel='closest jet pt', ylabel='number')

    signal = np.array([])
    background = np.array([])
    for i in range(len(z)):
        if (z[i,11] == 1):
            signal = np.append(signal,z[i,5])
        else:
            background = np.append(background,z[i,5])
    ax[1,1].hist(signal, bins=50, range=(0,200), histtype='step', color='r', density='True')
    ax[1,1].hist(background, bins=50, range=(0,200), histtype='step', color='b', density='True')
    ax[1,1].set(title='ptsum_opposite', xlabel='ptsum_opposite', ylabel='number')
    plt.tight_layout()
    plt.show()

elif (ichoose == 5):
    fig, ax = plt.subplots(2,2)
    signal = np.array([])
    background = np.array([])
    for i in range(len(z)):
        if (z[i,11] == 1):
            signal = np.append(signal,z[i,10])
        else:
            background = np.append(background,z[i,10])
    ax[0,0].hist(signal, bins=50, range=(0,2000), histtype='step', color='r', density='True')
    ax[0,0].hist(background, bins=50, range=(0,2000), histtype='step', color='b', density='True')
    ax[0,0].set(title='nmdt', xlabel='nmdt', ylabel='number')

    signal = np.array([])
    background = np.array([])
    for i in range(len(z)):
        if (z[i,11] == 1):
            signal = np.append(signal,z[i,11])
        else:
            background = np.append(background,z[i,11])
    ax[0,1].hist(signal, bins=50, range=(0,2000), histtype='step', color='r', density='True')
    ax[0,1].hist(background, bins=50, range=(0,2000), histtype='step', color='b', density='True')
    ax[0,1].set(title='nrpc', xlabel='nrpc', ylabel='number')

    signal = np.array([])
    background = np.array([])
    for i in range(len(z)):
        if (z[i,11] == 1):
            signal = np.append(signal,z[i,8])
        else:
            background = np.append(background,z[i,8])
    ax[1,0].hist(signal, bins=50, range=(0,200), histtype='step', color='r', density='True')
    ax[1,0].hist(background, bins=50, range=(0,200), histtype='step', color='b', density='True')
    ax[1,0].set(title='nmseg', xlabel='nmseg', ylabel='number')

    signal = np.array([])
    background = np.array([])
    for i in range(len(z)):
        if (z[i,11] == 1):
            signal = np.append(signal,z[i,7])
        else:
            background = np.append(background,z[i,7])
    ax[1,1].hist(signal, bins=50, range=(0,50), histtype='step', color='r', density='True')
    ax[1,1].hist(background, bins=50, range=(0,50), histtype='step', color='b', density='True')
    ax[1,1].set(title='nmstracklet', xlabel='nmstracklet', ylabel='number')

    plt.tight_layout()
    plt.show()

elif (ichoose == 6):
    fig, ax = plt.subplots(2,2)
    signal = np.array([])
    background = np.array([])
    for i in range(len(z)):
        if (z[i,11] == 1):
            signal = np.append(signal,z[i,6])
        else:
            background = np.append(background,z[i,6])
    ax[0,0].hist(signal, bins=50, range=(0,3.2), histtype='step', color='r', density='True')
    ax[0,0].hist(background, bins=50, range=(0,3.2), histtype='step', color='b', density='True')
    ax[0,0].set(title='met_dphi', xlabel='met_dphi', ylabel='number')

    signal = np.array([])
    background = np.array([])
    for i in range(len(z)):
        if (z[i,11] == 1):
            signal = np.append(signal,z[i,6])
        else:
            background = np.append(background,z[i,6])
    ax[0,1].hist(signal, bins=50, range=(0,3.2), histtype='step', color='r', density='True')
    ax[0,1].hist(background, bins=50, range=(0,3.2), histtype='step', color='b', density='True')
    ax[0,1].set(title='met_dphi', xlabel='met_dphi', ylabel='number')

    signal = np.array([])
    background = np.array([])
    for i in range(len(z)):
        if (z[i,11] == 1):
            signal = np.append(signal,z[i,6])
        else:
            background = np.append(background,z[i,6])
    ax[1,0].hist(signal, bins=50, range=(0,3.2), histtype='step', color='r', density='True')
    ax[1,0].hist(background, bins=50, range=(0,3.2), histtype='step', color='b', density='True')
    ax[1,0].set(title='met_dphi', xlabel='met_dphi', ylabel='number')

    signal = np.array([])
    background = np.array([])
    for i in range(len(z)):
        if (z[i,11] == 1):
            signal = np.append(signal,z[i,6])
        else:
            background = np.append(background,z[i,6])
    ax[1,1].hist(signal, bins=50, range=(0,3.2), histtype='step', color='r', density='True')
    ax[1,1].hist(background, bins=50, range=(0,3.2), histtype='step', color='b', density='True')
    ax[1,1].set(title='met_dphi', xlabel='met_dphi', ylabel='number')

    plt.tight_layout()
    plt.show()

elif (ichoose == 8):

    date = datetime.date.today()
    date = str(date)
    filename = 'xgb_var_region'+sregion+'-'+date+'.pdf'
    pp = PdfPages(filename)

    xgb.plot_importance(classifier)
    plt.tight_layout()
    pp.savefig()

    fig, ax = plt.subplots(2,2)
    print (np.shape(y_pred))
    ax[0,0].hist(y_pred[:], bins=50, range=(0,1.01), histtype='step', color='b', log='True')
    ax[0,0].set(title='y_pred', xlabel='y_pred', ylabel='number')
    ax[1,0].hist(y_pred_proba[:,1], bins=50, range=(0,1.01), histtype='step', color='r', log='True')
    ax[1,0].hist(y_pred_proba[:,0], bins=50, range=(0,1.01), histtype='step', color='b', log='True')
    ax[1,0].set(title='y_pred_prob', xlabel='y_pred_prob', ylabel='number')

# make numpy array for plotting
#    print (z_ytest[:])
    print ('y_test len',len(z_ytest))
    signal = np.array([])
    background = np.array([])
    for i in range(len(z_ytest)):
      if (z_ytest[i] == 1):
          signal = np.append(signal,y_pred_proba[i,1])
      elif (z_ytest[i] == 0):
          background = np.append(background,y_pred_proba[i,0])
      else:
          pass
    background = 1. - background
    ax[1,1].hist(signal, bins=50, range=(0,1.01), histtype='step', color='r', log='True')
    ax[1,1].hist(background, bins=50, range=(0,1.01), histtype='step', color='b', log='True')
    ax[1,1].set(title='y_pred_prob using y_test', xlabel='y_pred_prob using y_test', ylabel='number')
    plt.tight_layout()
    pp.savefig()

    fig, ax = plt.subplots(2,2)
    signal = np.array([])
    background = np.array([])
    for i in range(len(z)):
        if (z[i,11] == 1):
            signal = np.append(signal,z[i,0])
        else:
            background = np.append(background,z[i,0])
    ax[0,0].hist(signal, bins=50, range=(0,1), histtype='step', color='r', density='True')
    ax[0,0].hist(background, bins=50, range=(0,1), histtype='step', color='b', density='True')
    ax[0,0].set(title='ms tracklet mean', xlabel='ms tracklet mean', ylabel='number')

    signal = np.array([])
    background = np.array([])
    for i in range(len(z)):
        if (z[i,11] == 1):
            signal = np.append(signal,z[i,2])
        else:
            background = np.append(background,z[i,2])
    ax[0,1].hist(signal, bins=50, range=(0,1), histtype='step', color='r', density='True')
    ax[0,1].hist(background, bins=50, range=(0,1), histtype='step', color='b', density='True')
    ax[0,1].set(title='ms tracklet rms', xlabel='ms tracklet rms', ylabel='number')

    signal = np.array([])
    background = np.array([])
    for i in range(len(z)):
        if (z[i,11] == 1):
            signal = np.append(signal,z[i,1])
        else:
            background = np.append(background,z[i,1])
    ax[1,0].hist(signal, bins=50, range=(0,1), histtype='step', color='r', density='True')
    ax[1,0].hist(background, bins=50, range=(0,1), histtype='step', color='b', density='True')
    ax[1,0].set(title='mseg mean', xlabel='mseg mean', ylabel='number')
    signal = np.array([])
    background = np.array([])
    for i in range(len(z)):
        if (z[i,11] == 1):
            signal = np.append(signal,z[i,3])
        else:
            background = np.append(background,z[i,3])
    ax[1,1].hist(signal, bins=50, range=(0,1), histtype='step', color='r', density='True')
    ax[1,1].hist(background, bins=50, range=(0,1), histtype='step', color='b', density='True')
    ax[1,1].set(title='mseg rms', xlabel='mseg rms', ylabel='number')
    plt.tight_layout()
    pp.savefig()

    fig, ax = plt.subplots(2,2)
    signal = np.array([])
    background = np.array([])
    for i in range(len(z)):
        if (z[i,11] == 1):
            signal = np.append(signal,z[i,7])
        else:
            background = np.append(background,z[i,7])
    print (len(signal))
    print (len(background))
    ax[0,0].hist(signal, bins=50, range=(0,3), histtype='step', color='r', density='True')
    ax[0,0].hist(background, bins=50, range=(0,3), histtype='step', color='b', density='True')
    ax[0,0].set(title='jet drmin', xlabel='jet drmin', ylabel='number')

    signal = np.array([])
    background = np.array([])
    for i in range(len(z)):
        if (z[i,11] == 1):
            signal = np.append(signal,z[i,4])
        else:
            background = np.append(background,z[i,4])
    ax[0,1].hist(signal, bins=50, range=(0,3), histtype='step', color='r', density='True')
    ax[0,1].hist(background, bins=50, range=(0,3), histtype='step', color='b', density='True')
    ax[0,1].set(title='track drmin', xlabel='track drmin', ylabel='number')

    signal = np.array([])
    background = np.array([])
    for i in range(len(z)):
        if (z[i,11] == 1):
            signal = np.append(signal,z[i,10])
        else:
            background = np.append(background,z[i,10])
    ax[1,0].hist(signal, bins=50, range=(0,500), histtype='step', color='r', density='True')
    ax[1,0].hist(background, bins=50, range=(0,500), histtype='step', color='b', density='True')
    ax[1,0].set(title='closest jet pt', xlabel='closest jet pt', ylabel='number')

    signal = np.array([])
    background = np.array([])
    for i in range(len(z)):
        if (z[i,11] == 1):
            signal = np.append(signal,z[i,5])
        else:
            background = np.append(background,z[i,5])
    ax[1,1].hist(signal, bins=50, range=(0,200), histtype='step', color='r', density='True')
    ax[1,1].hist(background, bins=50, range=(0,200), histtype='step', color='b', density='True')
    ax[1,1].set(title='ptsum_opposite', xlabel='ptsum_opposite', ylabel='number')
    plt.tight_layout()
    pp.savefig()

    fig, ax = plt.subplots(2,2)
    signal = np.array([])
    background = np.array([])
    for i in range(len(z)):
        if (z[i,11] == 1):
            signal = np.append(signal,z[i,8])
        else:
            background = np.append(background,z[i,8])
    ax[0,0].hist(signal, bins=50, range=(0,2000), histtype='step', color='r', density='True')
    ax[0,0].hist(background, bins=50, range=(0,2000), histtype='step', color='b', density='True')
    ax[0,0].set(title='nmdt', xlabel='nmdt', ylabel='number')

    signal = np.array([])
    background = np.array([])
    for i in range(len(z)):
        if (z[i,11] == 1):
            signal = np.append(signal,z[i,9])
        else:
            background = np.append(background,z[i,9])
    ax[0,1].hist(signal, bins=50, range=(0,2000), histtype='step', color='r', density='True')
    ax[0,1].hist(background, bins=50, range=(0,2000), histtype='step', color='b', density='True')
    ax[0,1].set(title='nrpc', xlabel='nrpc', ylabel='number')

    signal = np.array([])
    background = np.array([])
    for i in range(len(z)):
        if (z[i,11] == 1):
            signal = np.append(signal,z[i,8])
        else:
            background = np.append(background,z[i,8])
    ax[1,0].hist(signal, bins=50, range=(0,200), histtype='step', color='r', density='True')
    ax[1,0].hist(background, bins=50, range=(0,200), histtype='step', color='b', density='True')
    ax[1,0].set(title='nmseg', xlabel='nmseg', ylabel='number')

    signal = np.array([])
    background = np.array([])
    for i in range(len(z)):
        if (z[i,11] == 1):
            signal = np.append(signal,z[i,9])
        else:
            background = np.append(background,z[i,9])
    ax[1,1].hist(signal, bins=50, range=(0,50), histtype='step', color='r', density='True')
    ax[1,1].hist(background, bins=50, range=(0,50), histtype='step', color='b', density='True')
    ax[1,1].set(title='nmstracklet', xlabel='nmstracklet', ylabel='number')
    plt.tight_layout()
    pp.savefig()

    fig, ax = plt.subplots(2,2)
    signal = np.array([])
    background = np.array([])
    for i in range(len(z)):
        if (z[i,11] == 1):
            signal = np.append(signal,z[i,6])
        else:
            background = np.append(background,z[i,6])
    ax[0,0].hist(signal, bins=50, range=(0,3.2), histtype='step', color='r', density='True')
    ax[0,0].hist(background, bins=50, range=(0,3.2), histtype='step', color='b', density='True')
    ax[0,0].set(title='met_dphi', xlabel='met_dphi', ylabel='number')

    signal = np.array([])
    background = np.array([])
    for i in range(len(z)):
        if (z[i,11] == 1):
            signal = np.append(signal,z[i,6])
        else:
            background = np.append(background,z[i,6])
    ax[0,1].hist(signal, bins=50, range=(0,3.2), histtype='step', color='r', density='True')
    ax[0,1].hist(background, bins=50, range=(0,3.2), histtype='step', color='b', density='True')
    ax[0,1].set(title='met_dphi', xlabel='met_dphi', ylabel='number')

    signal = np.array([])
    background = np.array([])
    for i in range(len(z)):
        if (z[i,11] == 1):
            signal = np.append(signal,z[i,6])
        else:
            background = np.append(background,z[i,6])
    ax[1,0].hist(signal, bins=50, range=(0,3.2), histtype='step', color='r', density='True')
    ax[1,0].hist(background, bins=50, range=(0,3.2), histtype='step', color='b', density='True')
    ax[1,0].set(title='met_dphi', xlabel='met_dphi', ylabel='number')

    signal = np.array([])
    background = np.array([])
    for i in range(len(z)):
        if (z[i,11] == 1):
            signal = np.append(signal,z[i,6])
        else:
            background = np.append(background,z[i,6])
    ax[1,1].hist(signal, bins=50, range=(0,3.2), histtype='step', color='r', density='True')
    ax[1,1].hist(background, bins=50, range=(0,3.2), histtype='step', color='b', density='True')
    ax[1,1].set(title='met_dphi', xlabel='met_dphi', ylabel='number')
    plt.tight_layout()
    pp.savefig()

# plot errors as a function of epoch
    results = classifier.evals_result()
    epochs = len(results['validation_0']['error'])
    print ('epochs',epochs)
    x_axis = range(epochs)

# plot log loss
    fig, ax = plt.subplots(2,2)
    ax[0,0].plot(x_axis, results['validation_0']['logloss'], label='Train')
    ax[0,0].plot(x_axis, results['validation_1']['logloss'], label='Test')
    ax[0,0].legend()
    ax[0,0].set(title='log loss', xlabel='epoch', ylabel='log loss')
# plot classification error
    ax[0,1].plot(x_axis, results['validation_0']['error'], label='Train')
    ax[0,1].plot(x_axis, results['validation_1']['error'], label='Test')
    ax[0,1].legend()
    ax[0,1].set(title='classification error', xlabel='epoch', ylabel='classification error')

# y_pred_proba already exists
    pos_pred_proba = y_pred_proba[:,1]
    fpr, tpr, _ = roc_curve(y_test,pos_pred_proba)
    ax[1,0].plot(tpr, fpr, label='xgb')
    ax[1,0].set(title='roc', xlabel='true positive', ylabel='false positive')

# abcd
    drmin = np.array([])
    xgb_score = np.array([])
    for i in range(1,len(mass_test_samples)):
        drmin = np.append(drmin,mass_test_samples[i].iloc[:,4])
    print (drmin.shape[0])
    for i in range (len(y_pred_proba_mass_list)):
        xgb_score = np.append(xgb_score,y_pred_proba_mass_list[i][:,1])
    print (xgb_score.shape[0])
    h = ax[1,1].hist2d(xgb_score, drmin, bins=50, norm=colors.LogNorm(), cmap='jet')
    ax[1,1].set_xlim(0,1)
    ax[1,1].set_ylim(0,4)
    ax[1,1].set(title='abcd', xlabel='bdt score', ylabel='gtrack drmin')
    plt.colorbar(h[3],ax=ax[1,1])

    plt.tight_layout()
    pp.savefig()

    fig, ax = plt.subplots(2,2)

# brute force
    drmin = np.array([])
    target = np.array([])
    xgb_score = np.array([])
    for i in range(1,len(mass_test_samples)):
        drmin = np.append(drmin,mass_test_samples[i].iloc[:,4])
        target = np.append(target,mass_test_samples[i].iloc[:,11])
    print (drmin.shape[0])
    for i in range (len(y_pred_proba_mass_list)):
        xgb_score = np.append(xgb_score,y_pred_proba_mass_list[i][:,1])
    print (xgb_score.shape[0])
    print (target.shape[0])
    h = ax[1,1].hist2d(xgb_score, drmin, bins=50, norm=colors.LogNorm(), cmap='jet')
    ax[1,1].set_xlim(0,1)
    ax[1,1].set_ylim(0,4)
    ax[1,1].set(title='abcd', xlabel='bdt score', ylabel='gtrack drmin')

    drmin_signal = np.array([])
    drmin_bkg = np.array([])
    xgb_score_signal = np.array([])
    xgb_score_bkg = np.array([])
    target_signal = np.array([])
    target_bkg = np.array([])
    for i in range(target.shape[0]):
        if (target[i] == 1):
            drmin_signal = np.append(drmin_signal,drmin[i])
            xgb_score_signal = np.append(xgb_score_signal,xgb_score[i])
            target_signal =  np.append(target_signal,target[i])
        elif (target[i] == 0):
            drmin_bkg = np.append(drmin_bkg,drmin[i])
            xgb_score_bkg = np.append(xgb_score_bkg,xgb_score[i])
            target_bkg =  np.append(target_bkg,target[i])
    print ('signal ',drmin_signal.shape[0])
    print ('background ',drmin_bkg.shape[0])
    print ('target signal ',target_signal.shape[0])
    print ('target background ',target_bkg.shape[0])

    h = ax[0,0].hist2d(xgb_score_signal, drmin_signal, bins=50, norm=colors.LogNorm(), cmap='jet')
    ax[0,0].set_xlim(0,1)
    ax[0,0].set_ylim(0,4)
    ax[0,0].set(title='abcd signal', xlabel='bdt score', ylabel='gtrack drmin')
    plt.colorbar(h[3],ax=ax[0,0])

    h = ax[0,1].hist2d(xgb_score_bkg, drmin_bkg, bins=50, norm=colors.LogNorm(), cmap='jet')
    ax[0,1].set_xlim(0,1)
    ax[0,1].set_ylim(0,4)
    ax[0,1].set(title='abcd bkg', xlabel='bdt score', ylabel='gtrack drmin')
    plt.colorbar(h[3],ax=ax[0,1])

    plt.tight_layout()
    pp.savefig()

    pp.close()

elif (ichoose == 9):
# plot classification error
    results = classifier.evals_result()
    epochs = len(results['validation_0']['error'])
    print ('epochs',epochs)
    x_axis = range(epochs)
 
# plot log loss
    fig, ax = plt.subplots(2,2)
    ax[0,0].plot(x_axis, results['validation_0']['logloss'], label='Train')
    ax[0,0].plot(x_axis, results['validation_1']['logloss'], label='Test')
    ax[0,0].legend()
    ax[0,0].set(title='log loss', xlabel='epoch', ylabel='log loss')
# plot classification error
    ax[0,1].plot(x_axis, results['validation_0']['error'], label='Train')
    ax[0,1].plot(x_axis, results['validation_1']['error'], label='Test')
    ax[0,1].legend()
    ax[0,1].set(title='classification error', xlabel='epoch', ylabel='classification error')

# y_pred_proba already exists
    pos_pred_proba = y_pred_proba[:,1]
    fpr, tpr, _ = roc_curve(y_test,pos_pred_proba)
    ax[1,0].plot(tpr, fpr, label='xgb')
    ax[1,0].set(title='roc', xlabel='true positive', ylabel='false positive')

# abcd
    drmin = np.array([])
    xgb_score = np.array([])
    for i in range(1,len(mass_test_samples)):
        drmin = np.append(drmin,mass_test_samples[i].iloc[:,4])
    print (drmin.shape[0])
    for i in range (len(y_pred_proba_mass_list)):
        xgb_score = np.append(xgb_score,y_pred_proba_mass_list[i][:,1])
    print (xgb_score.shape[0])
    h = ax[1,1].hist2d(xgb_score, drmin, bins=50, norm=colors.LogNorm(), cmap='jet')
    ax[1,1].set_xlim(0,1)
    ax[1,1].set_ylim(0,4)
    ax[1,1].set(title='abcd', xlabel='bdt score', ylabel='gtrack drmin')
    plt.colorbar(h[3],ax=ax[1,1])

    plt.tight_layout()
    plt.show()

elif (ichoose == 10):

    fig, ax = plt.subplots(2,2)

# brute force 
    drmin = np.array([])
    target = np.array([])
    xgb_score = np.array([])
    for i in range(1,len(mass_test_samples)):
        drmin = np.append(drmin,mass_test_samples[i].iloc[:,4])
        target = np.append(target,mass_test_samples[i].iloc[:,11])
    print (drmin.shape[0])
    for i in range (len(y_pred_proba_mass_list)):
        xgb_score = np.append(xgb_score,y_pred_proba_mass_list[i][:,1])
    print (xgb_score.shape[0])
    print (target.shape[0])
    h = ax[1,1].hist2d(xgb_score, drmin, bins=50, norm=colors.LogNorm(), cmap='jet')
    ax[1,1].set_xlim(0,1)
    ax[1,1].set_ylim(0,4)
    ax[1,1].set(title='abcd', xlabel='bdt score', ylabel='gtrack drmin')
    plt.colorbar(h[3],ax=ax[1,1])

    drmin_signal = np.array([])
    drmin_bkg = np.array([])
    xgb_score_signal = np.array([])
    xgb_score_bkg = np.array([])
    target_signal = np.array([])
    target_bkg = np.array([])
    for i in range(target.shape[0]):
        if (target[i] == 1):
            drmin_signal = np.append(drmin_signal,drmin[i])
            xgb_score_signal = np.append(xgb_score_signal,xgb_score[i])
            target_signal =  np.append(target_signal,target[i])
        elif (target[i] == 0):
            drmin_bkg = np.append(drmin_bkg,drmin[i])
            xgb_score_bkg = np.append(xgb_score_bkg,xgb_score[i])
            target_bkg =  np.append(target_bkg,target[i])
    print ('signal ',drmin_signal.shape[0])
    print ('background ',drmin_bkg.shape[0])
    print ('target signal ',target_signal.shape[0])
    print ('target background ',target_bkg.shape[0])

    h = ax[0,0].hist2d(xgb_score_signal, drmin_signal, bins=50, norm=colors.LogNorm(), cmap='jet')
    ax[0,0].set_xlim(0,1)
    ax[0,0].set_ylim(0,4)
    ax[0,0].set(title='abcd signal', xlabel='bdt score', ylabel='gtrack drmin')
    plt.colorbar(h[3],ax=ax[0,0])

    h = ax[0,1].hist2d(xgb_score_bkg, drmin_bkg, bins=50, norm=colors.LogNorm(), cmap='jet')
    ax[0,1].set_xlim(0,1)
    ax[0,1].set_ylim(0,4)
    ax[0,1].set(title='abcd bkg', xlabel='bdt score', ylabel='gtrack drmin')
    plt.colorbar(h[3],ax=ax[0,1])

# brute force for now
    asum = 0
    bsum = 0
    csum = 0
    dsum = 0
    esum = 0
    fsum = 0
    for i in range (drmin_bkg.shape[0]):
      if (drmin_bkg[i] >= 0.7 and xgb_score_bkg[i] >= 0.75): asum = asum + 1
      if (drmin_bkg[i] < 0.7 and xgb_score_bkg[i] >= 0.75): bsum = bsum + 1
      if (drmin_bkg[i] >= 0.7 and xgb_score_bkg[i] < 0.75): csum = csum + 1
      if (drmin_bkg[i] < 0.7 and xgb_score_bkg[i] < 0.75): dsum = dsum + 1
      if (drmin_bkg[i] >= 0.7 and xgb_score_bkg[i] >= 0.75 and (target_bkg[i] == 0)): esum = esum + 1
    print (asum, bsum, csum, dsum, (asum + bsum + csum + dsum))
    esum_abcd = float(csum) * float(bsum) / float(dsum)
    print (esum, esum_abcd)


    plt.tight_layout()
    plt.show()


else:
    pass

print ('done')
