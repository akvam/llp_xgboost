# llp_xgboost

Using xgboost for the 1 MSVtx llp search

llp_jets_050321.py creates the .csv samples from llp signal, mc jets, and data
xgboost_atlas_all.py uses xgboost to separate signal from background
python xgboost_atlas_all.py

3.8.8 (default, Apr 13 2021, 19:58:26)
[GCC 7.3.0]
1.4.2
1.2.4
0.24.1


